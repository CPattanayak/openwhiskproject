import os

def inject_variables(args):
    # inject params as environmental variables
    os.environ['MONGO_URL'] = args['MONGO_URL']

    # remove bound variables from the payload to prevent tracing values
    del args['MONGO_URL']

def triggerCustomer(args):
    inject_variables(args)
    from service.CustomerService import handle
    return handle(args)

def triggerAddress(args):
    inject_variables(args)
    from service.AddressService import handle
    return handle(args)

def triggerCustomerDelete(args):
    inject_variables(args)
    from service.CustomerDeleteHandler import handle
    return handle(args)

