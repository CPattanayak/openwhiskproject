#!/bin/bash
#
# Script to download and deploy and openwhisk actions
#
# prepare wsk client for usage
WORKDIR=${WORKDIR:=tmp/}
WSKCLI_URI=${WSKCLI_URI:=https://github.com/apache/incubator-openwhisk-cli/releases/download/0.9.0-incubating/OpenWhisk_CLI-0.9.0-incubating-linux-amd64.tgz}
ZIP_NAME=${ZIP_NAME=func.zip}
# update apt
apt-get -q update --assume-yes 2> /dev/null
if [[ $? -ne 0 ]]; then
    echo "apt update failed"
    exit 1
fi

apt-get -q install wget --assume-yes 2> /dev/null

if [[ $? -ne 0 ]]; then
    echo "failed to install wget"
    exit 1
fi
apt-get update && apt-get install zip -y
zip -r $ZIP_NAME . -x *.zip  -x '*.git*' -x '*.vscode*' -x '*test*' -x '*.sh'

mkdir -p $WORKDIR

if [[ $? -ne 0 ]]; then
    echo "failed to create '$WORKDIR'"
    exit 1
fi

# append wsk to the path
PATH=$PATH:$WORKDIR

# move into the workdir
cd $WORKDIR

# download the wsk cli
echo "Downloading wsk cli"
wskcliZipFileName=OpenWhisk_CLI-0.9.0-incubating-linux-amd64.tgz
wget -q --output-document=$wskcliZipFileName $WSKCLI_URI
if [[ $? -ne 0 ]]; then
    echo "failed to download OpenWisk CLI from '$WSKCLI_URI'"
    exit 1
fi

# extract wsk cli
tar xvzf $wskcliZipFileName

# copy wsk client to /usr/local/bin and remove WORKDIR
cp ./wsk /usr/local/bin/wsk
cd ..
echo "list of file present"
ls -lrt
wsk property set --apihost $OW_URL
wsk property set --auth $OW_AUTH
wsk property set --namespace guest -i

retrievedPackageName=$(wsk package list -i)
if [[ $? -ne 0 ]]; then
    echo "failed to retrieve package list from wsk"
    exit 1
fi
if [[ -z "$(echo $retrievedPackageName | grep $OW_PACKAGE_NAME)" ]]; then
    echo "Package '$OW_PACKAGE_NAME' create"
    wsk package create $OW_PACKAGE_NAME -i;
else
    echo "Package '$OW_PACKAGE_NAME'already exists"
fi

# Create binding package (if does not exist yet)
if [[ -z  $(wsk package list -i | grep "$OW_PACKAGE_BINDING_NAME ") ]]; then wsk package bind $OW_PACKAGE_NAME $OW_PACKAGE_BINDING_NAME -i;  echo "Package binding";fi
# Update binding arguments
wsk package update $OW_PACKAGE_BINDING_NAME --param MONGO_URL "$CI_MDB_PRICER_RW" -i;


echo "Deploying function packages..."
wsk action update $OW_PACKAGE_NAME/triggerCustomer --kind $FUNCTION_BASE_IMAGE ${ZIP_NAME} --main triggerCustomer  --timeout 300000 -i
wsk action update $OW_PACKAGE_NAME/triggerAddress --kind $FUNCTION_BASE_IMAGE ${ZIP_NAME} --main triggerAddress  --timeout 300000 -i
wsk action update $OW_PACKAGE_NAME/triggerCustomerDelete --kind $FUNCTION_BASE_IMAGE ${ZIP_NAME} --main triggerCustomerDelete  --timeout 300000 -i
# an array of tuples of: function, functionEntry

# register the action chains
echo "Chain Actions"

echo "Register $OW_PACKAGE_NAME/customerWriter"
wsk action update $OW_PACKAGE_NAME/customerSequence --sequence $OW_PACKAGE_BINDING_NAME/triggerCustomer,$OW_PACKAGE_BINDING_NAME/triggerAddress --timeout 300000 -i

echo "Register $OW_PACKAGE_NAME/customerDelete"
wsk action update $OW_PACKAGE_NAME/customerDeleteSequence --sequence $OW_PACKAGE_BINDING_NAME/triggerCustomerDelete --timeout 300000 -i


echo "Retrieve listing of currently deployed actions..."
wsk list -i

echo "List content of the binding package"
wsk package get $OW_PACKAGE_BINDING_NAME --summary -i