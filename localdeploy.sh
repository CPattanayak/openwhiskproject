#!/bin/bash
#
# Script to download and deploy and openwhisk actions
#
# prepare wsk client for usage

export OW_PACKAGE_NAME=custpackage
export OW_PACKAGE_BINDING_NAME=custbind

export CI_MDB_PRICER_RW=mongodb://172.28.97.161:27017
export CI_NEO4J_UID=neo4j
export CI_NEO4J_PWD=admin
export NEO4J_URL=bolt://172.28.97.161:7687
export REDIS_HOST=bolt://172.28.97.161:7687
export REDIS_DB_ID=intakedb

export FUNCTION_BASE_IMAGE=python:koa



# prepare wsk client for usage
wsk property set --apihost 13.71.83.23:443
wsk property set --auth 23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP 
wsk property set --namespace guest -i

retrievedPackageName=$(wsk package list -i)
if [[ $? -ne 0 ]]; then
    echo "failed to retrieve package list from wsk"
    exit 1
fi
if [[ -z "$(echo $retrievedPackageName | grep $OW_PACKAGE_NAME)" ]]; then
    echo "Package '$OW_PACKAGE_NAME' create"
    wsk package create $OW_PACKAGE_NAME -i;
else
    echo "Package '$OW_PACKAGE_NAME'already exists"
fi

# Create binding package (if does not exist yet)
if [[ -z  $(wsk package list -i | grep "$OW_PACKAGE_BINDING_NAME ") ]]; then wsk package bind $OW_PACKAGE_NAME $OW_PACKAGE_BINDING_NAME -i;  echo "Package binding";fi
# Update binding arguments
wsk package update $OW_PACKAGE_BINDING_NAME --param MONGO_URL "$CI_MDB_PRICER_RW" -i;


echo "Deploying function packages..."
wsk action update $OW_PACKAGE_NAME/triggerCustomer --kind $FUNCTION_BASE_IMAGE ./cust.zip --main triggerCustomer  --timeout 300000 -i
wsk action update $OW_PACKAGE_NAME/triggerAddress --kind $FUNCTION_BASE_IMAGE ./cust.zip --main triggerAddress  --timeout 300000 -i
# an array of tuples of: function, functionEntry

# register the action chains
echo "Chain Actions"

echo "Register $OW_PACKAGE_NAME/customerWriter"
wsk action update $OW_PACKAGE_NAME/customerSequence --sequence $OW_PACKAGE_BINDING_NAME/triggerCustomer,$OW_PACKAGE_BINDING_NAME/triggerAddress --timeout 300000 -i

echo "Retrieve listing of currently deployed actions..."
wsk list -i

echo "List content of the binding package"
wsk package get $OW_PACKAGE_BINDING_NAME --summary -i