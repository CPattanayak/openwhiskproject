from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
import os
import hashlib


_url = os.environ['MONGO_URL']

def write_customer_detail(customerDetail):
    client = MongoClient(_url)
    db = client['Customer']

    db.customerDetail.replace_one({ 'id': customerDetail['id']}, customerDetail, upsert=True)
    client.close()

def write_customer_address(customerAddress):
    client = MongoClient(_url)
    db = client['Customer']

    db.customerAddressDetail.replace_one({'id': customerAddress['id']}, customerAddress, upsert=True)
    client.close()

def delete_customer_data(customerId):
    _url = os.environ['MONGO_URL']
    client = MongoClient(_url)
    db = client['Customer']
    db.customerDetail.delete_many({'id': customerId})
    db.customerAddressDetail.delete_many({'customerId': customerId})
    client.close()



