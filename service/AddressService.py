import json
import rawWriter.MongoWriter as mongoWriter
def handle(data,context=None):
    address = data['addresses']
    cust_id = data['customerId']
    add_id_list =[]
    for custAdd in address:
        add_id_list.append(custAdd['id'])
        custAdd['customerId'] = cust_id
        print('DATA TYPE: {0}, DATA_VALUE: {1}'.format(type(data), json.dumps(custAdd)))
        mongoWriter.write_customer_address(custAdd)
    return {'customerId': cust_id,'addresses':add_id_list}